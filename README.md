# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## Author: Taylor C. Brennan, tbrennan@uoregon.edu ##

## Purpose ## 

This is designed to calculate the controls times for a brevet race. This has a running server and proccess JSON requests. By adding the control distance for a brevet, it will give the open and close times. Those entered distances are validated by checking if it is within the allowed range for the brevet and that only a float was passed in. It will calculate the results and return the control times where the clients page updates the fields with the times. 

A submit and Display buttons have been added. The submit button will go though the table of values and attempt to submit to the server. If the distance is less then the previous accepted entry, it will be skipped. You will need to enter in values in the correct order. The Display button will redirect the page to diplay the database formatted on HTML.

## ACP controle times

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). 
As noticed from the calculator, 
-any control past the brevet distance is equal to the brevent distance.
-can only have controls up to 120% of brevet (ie. a 200km brevet can have a max of 240KM) Validation for this is handled though app.py
-can only have floats.  Validation for this is handled though app.py
-closing times before 15KM will add an hr (count for open time) like the calculator, it it not suggested to have controls so close to the start
-in both open and close, time are calculated based on the distance range. (0-200) (200-400) etc. So for 400KM, the first 200kM will be calculated by the rules of 0-200 and the second 200km will be calculated with the rules of 200-400.

Open times are calulated by [(200 , 34), (400, 32), (600, 30), (1000, 28), (1300, 26)]
-The first 200km are calculated if you were going 34 km/hr, 200-400km are calculated if you were going 32km/hr. So the total for the 400km point is 200km at 34km/hr + 200km at 32km/hr
-There is a weird case with 200km, where it will add 10min to fix the time.

## Submitting and Displaying

-If there is an illegal character, then the distance is set to 0 
-When submitting, if the previous value is larger then the current value, the current value will be skipped.


## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

The implementation that you will do will fill in times as the input fields are filled using Ajax and Flask. Currently the miles to km (and vice versa) is implemented with Ajax. You'll extend that functionality as follows:

* Each time a distance is filled in, the corresponding open and close times should be filled in with Ajax.   

* If out of range or bad characted are used, the open and close field will be blank, and there will be an error message at the bottom of the table

## Testing

There is a test_acp_time.py that runs the current test. Nose testing for the JSON responce is currently being worked on.

In the testing, there is a copied control distance from the provided sites to validate.

I did not add testing for project 5.

## Tasks

You'll turn in your credentials.ini using which we will get the following:

* The working application.

* A README.md file that includes not only identifying information (your name) but but also a revised, clear specification 
  of the brevet controle time calculation rules.

* Dockerfile

* Test cases for the two buttons. No need to run nose.

* docker-compose.yml

## Grading Rubric

* If your code works as expected: 100 points. This includes:
	* AJAX in the frontend. That is, open and close times are automatically populated, 
	* Frontend to backend interaction (with correct requests/responses), 
	* README is updated with your name and email.

* If the AJAX logic is not working, 10 points will be docked off. 

* If the README is not clear or missing, up to 15 points will be docked off. 

* If the two test cases fail, up to 15 points will be docked off. 

* If the logic to enter into or retrieve from the database is wrong, 30 points will be docked off.

* If none of the functionalities work, 30 points will be given assuming 
    * The credentials.ini is submitted with the correct URL of your repo, and
    * Dockerfile is present 
    * Docker-compose.yml works/builds without any errors 

* If the Docker-compose.yml doesn't build or is missing, 10 points will be docked off.

* If credentials.ini is missing, 0 will be assigned.
