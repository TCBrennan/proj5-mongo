import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import flask
import arrow
import acp_times  # Brevet time calculations
import logging

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")

    #values are predetermined 
    total_d = request.args.get('total_dis', 999, type=float)
    start_t = request.args.get('start_time', " ", type=str)

    #Comes from KM field. Users could enter characters that isn't a float,
    #This take in the request as a string, checks if it can be converted
    #into a float(if yes, convert; if no km=0) then verifies if the float
    #is within the +20% range (based of brevet distance). 
    km = request.args.get('km', type=str)
    good_value = isfloat(km)
    if good_value:
        km = float(km)
    else:
        km = 0
    in_range = (km <= (1.2*total_d))

    #log values from JSON request
    app.logger.debug("km={}".format(km))
    app.logger.debug("total_dis={}".format(total_d))
    app.logger.debug("start_time={}".format(start_t))
    app.logger.debug("request.args: {}".format(request.args))

    #get calculation from acp_time.py 
    open_time = acp_times.open_time(km, total_d, start_t)
    close_time = acp_times.close_time(km, total_d, start_t)

    #result are: open time, close time, is this within range?, is this a float?
    result = {"open": open_time, "close": close_time,
     "in_range": in_range, "good_value": good_value}
    return flask.jsonify(result=result)

#simple function to check if it is a float    
def isfloat(km):
    try:
        float(km)
        return True
    except ValueError:
        return False

@app.route('/new', methods=['POST'])
def new():
    item_doc = {
    'miles': request.form['miles'], 'km':request.form['km'],
     'loc':request.form['locat'], 'open_t': request.form['open'], 
    'close_t': request.form['close'], 'notes':request.form['note']
    }
    db.tododb.insert_one(item_doc)

    rest= {"passes": "Yes"}
    return flask.jsonify(result=rest)

@app.route('/display')
def display():
    return redirect(url_for('todo'))

@app.route('/todo')
def todo():

    _items = db.tododb.find()
    items = [item for item in _items]
    return render_template('todo.html', items=items)




#############


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)